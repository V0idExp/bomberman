CFLAGS:= $(CFLAGS) -ggdb -Wall -std=gnu99 \
         $(shell pkg-config --cflags glib-2.0) \
         $(shell sdl2-config --cflags) \
         $(shell pkg-config --cflags SDL2_image) \
	 -I$(PWD)/../spark/include \
         -I$(PWD)/include \
	 $(shell for dir in $$(ls -d $(PWD)/deps/*/); do echo "-I$${dir} -I$${dir}include"; done)

LDFLAGS:= $(LDFLAGS) $(LIBS) \
	  $(shell pkg-config --libs glib-2.0) \
          $(shell sdl2-config --libs) \
          $(shell pkg-config --libs SDL2_image) \
	  -L$(PWD)/../spark/src -Wl,-rpath,$(PWD)/../spark/src \
	  $(shell for dir in $$(ls -d $(PWD)/deps/*/); do echo "-L$${dir} -Wl,-rpath,$${dir}"; done) \
	  -lgame -lchipmunk -lAStar

ROOT_DIR= $(PWD)

export CFLAGS
export LDFLAGS
export ROOT_DIR

all:
	echo $(CFLAGS)
	$(MAKE) -C deps/
	$(MAKE) -C src/

clean:
	$(MAKE) -C src/ clean

distclean:
	$(MAKE) -C deps/ clean
	$(MAKE) -C src/ clean
