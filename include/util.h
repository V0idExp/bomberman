#pragma once

#define strings_array_init(...) {__VA_ARGS__, NULL}

void
assert_game_has_systems(const ComponentType *deps);

struct RefCounter;
typedef struct RefCounter RefCounter;

RefCounter*
ref_counter_new(void *data, void (*destructor)(void*));

RefCounter*
ref_counter_inc(RefCounter *rc);

void
ref_counter_dec(RefCounter *rc);

void*
ref_counter_get(RefCounter *rc);
