/******************************************************************************
	Body system (collision detection)
******************************************************************************/

#pragma once

#include "game.h"
#include "common.h"
#include <stdbool.h>

enum {
	// contains struct Body as payload
	EVENT_COLLISION = 1
};

typedef enum {
	BODY_TYPE_STATIC = 1,
	BODY_TYPE_MOVABLE
} BodyType;

struct Body {
	Entity entity;
	Rect rect;
	BodyType type;
	unsigned int group;
};

void
body_system_register();

void
body_init(Entity e, BodyType t, Rect *r, unsigned int group);

void
body_set_position(Entity e, int x, int y);

bool
body_get_nearest(int x, int y, struct Body *dst);

void
body_get_inside_rect(Rect r, struct Body **result_set, size_t *size);
