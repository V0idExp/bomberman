#pragma once

#include "common.h"
#include "game.h"

/******************************************************************************
	Bomberman
******************************************************************************/
enum {
	// has struct PathNode as event payload
	EVENT_BOMBERMAN_MOVED = 1,
	EVENT_BOMBERMAN_KILLED
};

void
bomberman_system_register();

Entity
bomberman_create(Entity battlefield);


/******************************************************************************
	Bombs
******************************************************************************/
enum {
	EVENT_BOMB_EXPLOSION = 1
};

void
bomb_system_register();

void
bomb_system_init(Entity battlefield);

Entity
bomb_drop(Entity owner, int x, int y, int radius, int timer);
