#pragma once

#include "game.h"

enum {
	BOT_TYPE0,
	BOT_TYPE1,
	BOT_TYPE2
};

enum {
	EVENT_BOT_KILLED = 1
};

void
bot_system_register();

Entity
bot_create(int type, Entity map, int x, int y);
