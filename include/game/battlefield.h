/******************************************************************************
	Battlefield
******************************************************************************/

#pragma once

#include "game.h"

#define BATTLEFIELD_ROWS 25
#define BATTLEFIELD_COLS 33
#define TILE_SIZE 24

enum {
	TERRAIN_WALL,
	TERRAIN_GROUND,
	TERRAIN_BRICK,
	TERRAIN_BLOOD
};

Entity
battlefield_create();

int
battlefield_terrain_at_point(Entity battlefield, int x, int y);

void
battlefield_set_terrain_at_point(Entity battlefield, int type, int x, int y);
