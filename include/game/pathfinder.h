#pragma once

#include "game.h"

struct PathNode {
	int x;
	int y;
};

struct Path;

struct Path*
path_find(Entity map, int x1, int y1, int x2, int y2);

struct Path*
path_random(Entity map, int x1, int y1);

size_t
path_len(struct Path *path);

struct PathNode*
path_node_get(struct Path *path, size_t index);

void
path_free(struct Path *path);
