#pragma once

#include "game.h"
#include <SDL_platform.h>

// Rect type
#ifdef __MACOSX__
#include <MacTypes.h>
#else
typedef struct {
	short top, left, bottom, right;
} Rect;
#endif

#define rect_width(__rect) (__rect.right - __rect.left)
#define rect_height(__rect) (__rect.bottom - __rect.top)
#define rect_ptr_width(__rect) (__rect->right - __rect->left)
#define rect_ptr_height(__rect) (__rect->bottom - __rect->top)

// component types
enum {
	CORE = GAME_CORE + 1,
	INPUT,
	BODY,
	GFX,
	IMAGE,
	SPRITE,
	BOMBERMAN,
	TILEMAP,
	TIMER,
	BOMB,
	BOT
};

// body types
enum {
	BODY_BOMB = 1,
	BODY_BOMBERMAN,
	BODY_BOT,
	BODY_FIREBALL,
	BODY_TERRAIN
};

// number of bots
#define BOTS_COUNT 15
