#pragma once

enum {
	EVENT_KEYPRESS = 1,
	EVENT_KEYRELEASE,
	EVENT_MOUSEPRESS,
	EVENT_MOUSERELEASE
};

void
input_system_register();
