#include "body.h"
#include "game/bomberman.h"
#include "game/pathfinder.h"
#include "input.h"
#include "mapped_pool.h"
#include "sprite.h"
#include "tilemap.h"
#include <SDL_events.h>
#include <SDL_timer.h>
#include <glib.h>

// bit manipulation macros
#define SET_BIT(var, flag) var |= flag
#define CLR_BIT(var, flag) var &= ~flag
#define HAS_BIT(var, flag) var & flag

// walk speed in pixels per second
#define WALK_SPEED 50
#define BOMB_TIMEOUT 4000

enum {
	WALK_UP    = 1,
	WALK_DOWN  = 2,
	WALK_LEFT  = 4,
	WALK_RIGHT = 8
};

/**
 * Bomberman states.
 */
enum {
	STATE_REST,
	STATE_WALK,
	STATE_DEAD
};

struct Bomberman {
	Entity entity;
	Entity battlefield;

	// current state information
	int state;

	// current position
	int x;
	int y;

	// movement offset
	int dx;
	int dy;

	// destination position
	int dst_x;
	int dst_y;

	// walk direction
	int current_walk_dir;
	int walk_dir;

	// set when bomberman's body collided with other bodies
	bool collided;

	// when set, bomberman will try to place a bomb
	bool drop_bomb;

	// info about just dropped bomb for temporary collision ignoring
	struct {
		Entity entity;
		bool collided;
	} dropped_bomb;

	// number of bombs remaining
	int bombs_left;

	// dropped bombs
	GHashTable *bombs;

	// current strength of bomb explosions
	int bomb_radius;

	// last update timestamp
	Uint32 last_update;
};

static void
set_state_rest(struct Bomberman *b)
{
	b->state = STATE_REST;
	b->walk_dir = 0;
	b->current_walk_dir = 0;
	b->dx = b->dy = 0;
	b->dst_x = b->dst_y = 0;
	b->collided = false;

	sprite_animation_pause(b->entity);
	sprite_position_set(b->entity, b->x, b->y);
	body_set_position(b->entity, b->x, b->y);
}

static void
set_state_walk(struct Bomberman *b)
{
	b->state = STATE_WALK;
	b->last_update = 0;

	char *anim = NULL;
	int current_walk_dir = 0;

	// compute "snapped" current coordinates
	struct Tile *t = tilemap_tile_at_point(b->battlefield, b->x, b->y);
	int x = t->rect.left + rect_width(t->rect) / 2;
	int y = t->rect.top + rect_height(t->rect) / 2;
	int w = rect_width(t->rect);
	int h = rect_height(t->rect);

	// determine the animation corresponding to the active walking direction
	// and set the destination coordinates
	if (HAS_BIT(b->walk_dir, WALK_UP)) {
		anim = "up";
		current_walk_dir = WALK_UP;
		b->dst_x = x;
		b->dst_y = y - h;
	}
	else if (HAS_BIT(b->walk_dir, WALK_DOWN)) {
		anim = "down";
		current_walk_dir = WALK_DOWN;
		b->dst_x = x;
		b->dst_y = y + h;
	}
	else if (HAS_BIT(b->walk_dir, WALK_LEFT)) {
		anim = "left";
		current_walk_dir = WALK_LEFT;
		b->dst_x = x - w;
		b->dst_y = y;
	}
	else if (HAS_BIT(b->walk_dir, WALK_RIGHT)) {
		anim = "right";
		current_walk_dir = WALK_RIGHT;
		b->dst_x = x + w;
		b->dst_y = y;
	}

	// play animation from beginning only if it's currently paused or walk
	// direction changed, to avoid glitches on continuous movements
	if (sprite_animation_is_paused(b->entity) ||
	    b->current_walk_dir != current_walk_dir) {
		sprite_animation_play(b->entity, anim);
		b->current_walk_dir = current_walk_dir;
	}
}

static void
handle_dead_anim_finish(struct Event *evt, void *data)
{
	game_event_push(
		BOMBERMAN,
		EVENT_BOMBERMAN_KILLED,
		evt->entity,
		NULL,
		0
	);
}

static void
set_state_dead(struct Bomberman *b)
{
	b->state = STATE_DEAD;
	sprite_animation_play(b->entity, "dead");
	game_event_subscribe(
		SPRITE,
		EVENT_ANIMATION_FINISHED,
		b->entity,
		handle_dead_anim_finish,
		NULL
	);
}

static bool
update_movement(struct Bomberman *b)
{
	// update bomberman and sprite position and reset movement deltas
	b->x += b->dx;
	b->y += b->dy;
	b->dx = b->dy = 0;
	sprite_position_set(b->entity, b->x, b->y);

	// stop if no movement has to be done
	if (b->x == b->dst_x && b->y == b->dst_y)
		return false;

	// compute time delta between current timestamp and last
	// bomberman update
	Uint32 ts = SDL_GetTicks();
	if (!b->last_update)
		b->last_update = ts;
	Uint32 delta_t = ts - b->last_update;

	// compute pixel time as milliseconds per pixel and update
	// bomberman's position delta based on walking direction
	float pixel_time = 1000.0f / WALK_SPEED;

	// compute the movement delta in pixels by dividing the time delta by
	// pixel time
	int delta = delta_t / pixel_time;

	// consume movement delta 1px at time, so that we don't miss the
	// destination
	while (delta > 0) {
		b->last_update = ts;

		switch (b->current_walk_dir) {
		case WALK_UP:
			b->dy--;
			break;
		case WALK_DOWN:
			b->dy++;
			break;
		case WALK_LEFT:
			b->dx--;
			break;
		case WALK_RIGHT:
			b->dx++;
		}

		if (b->x + b->dx == b->dst_x && b->y + b->dy == b->dst_y)
			break;

		delta--;
	}

	// move the body, effectively probing the way: if a collision between
	// this and next update occurs, the real bomberman and sprite
	// coordinates won't be updated
	body_set_position(b->entity, b->x + b->dx, b->y + b->dy);

	return true;
}

static void
handle_own_bomb_explosion(struct Event *evt, void *data)
{
	Entity e = *(Entity*)data;
	struct Bomberman *b = game_entity_component_get(e, BOMBERMAN);
	b->bombs_left++;

	g_hash_table_remove(b->bombs, GUINT_TO_POINTER(evt->entity));
}

static void
drop_bomb(struct Bomberman *b)
{
	if (b->bombs_left == 0 || b->dropped_bomb.entity)
		return;

	// compute the "snapped" droping coordinates
	struct Tile *t = tilemap_tile_at_point(b->battlefield, b->x, b->y);
	int x = t->rect.left + rect_width(t->rect) / 2;
	int y = t->rect.top + rect_height(t->rect) / 2;

	struct Body body;

	if (!body_get_nearest(x, y, &body) || body.entity == b->entity) {
		Entity bomb = bomb_drop(
			b->entity, x, y, b->bomb_radius, BOMB_TIMEOUT
		);

		g_hash_table_add(b->bombs, GUINT_TO_POINTER(bomb));

		game_event_subscribe(
			BOMB,
			EVENT_BOMB_EXPLOSION,
			bomb,
			handle_own_bomb_explosion,
			&b->entity
		);

		b->dropped_bomb.entity = bomb;
		b->dropped_bomb.collided = true;
		b->bombs_left--;
	}
}

static void
handle_keys(struct Event *evt, void *data)
{
	Entity e = GPOINTER_TO_UINT(data);
	struct Bomberman *b = game_entity_component_get(e, BOMBERMAN);
	struct SDL_KeyboardEvent *key_event = evt->data;

	// determine the walk direction from the keypress/keyrelease
	int walk_dir = -1;
	switch (key_event->keysym.sym) {
		case SDLK_w:
			walk_dir = WALK_UP;
			break;
		case SDLK_a:
			walk_dir = WALK_LEFT;
			break;
		case SDLK_s:
			walk_dir = WALK_DOWN;
			break;
		case SDLK_d:
			walk_dir = WALK_RIGHT;
			break;
	}

	if (evt->event == EVENT_KEYPRESS) {
		if (walk_dir != -1)
			SET_BIT(b->walk_dir, walk_dir);
		else if (key_event->keysym.sym == SDLK_SPACE)
			b->drop_bomb = true;
	}
	else if (evt->event == EVENT_KEYRELEASE) {
		if (walk_dir != -1)
			CLR_BIT(b->walk_dir, walk_dir);
		else if (key_event->keysym.sym == SDLK_SPACE)
			b->drop_bomb = false;
	}
}

static void
handle_collisions(struct Event *evt, void *data)
{
	Entity e = evt->entity;
	struct Bomberman *b = game_entity_component_get(e, BOMBERMAN);
	struct Body *obstacle = evt->data;

	if (obstacle->group == BODY_BOMB &&
	    b->dropped_bomb.entity == obstacle->entity) {
		b->dropped_bomb.collided = true;
		return;
	}

	if ((obstacle->group == BODY_FIREBALL || obstacle->group == BODY_BOT) &&
	    b->state != STATE_DEAD) {
		set_state_dead(b);
		return;
	}

	b->collided = true;
}


static void
sys_init(struct System *self)
{
	const ComponentType deps[] = {
		BODY, INPUT, SPRITE, 0
	};
	assert_game_has_systems(deps);
	self->data = mapped_pool_new(sizeof(struct Bomberman), 1, 1);
}

static void
sys_exit(struct System *self)
{
	mapped_pool_free(self->data);
}


static void
sys_update(struct System *self)
{
	PoolIter i;
	pool_iter_init(mapped_pool_storage_get(self->data), &i);

	while (pool_iter_next(&i)) {
		struct Bomberman *b = pool_iter_get(&i);

		// bomb placement and collision logic
		if (b->dropped_bomb.entity) {
			if (b->dropped_bomb.collided)
				b->dropped_bomb.collided = false;
			else
				b->dropped_bomb.entity = 0;
		}
		else if (b->drop_bomb)
			drop_bomb(b);

		// rest and walk states switching logic
		bool notify = false;
		if (b->state == STATE_REST && b->walk_dir) {
			set_state_walk(b);
		}
		else if (b->state == STATE_WALK && b->collided) {
			set_state_rest(b);
			notify = true;
		}
		else if (b->state == STATE_WALK && !update_movement(b)) {
			if (b->walk_dir)
				set_state_walk(b);
			else
				set_state_rest(b);

			notify = true;
		}

		if (notify) {
			struct PathNode *node = g_new0(struct PathNode, 1);
			node->x = b->x;
			node->y = b->y;

			game_event_push(
				BOMBERMAN,
				EVENT_BOMBERMAN_MOVED,
				b->entity,
				node,
				sizeof(struct PathNode)
			);
		}
	}
}

static void
sys_create_component(struct System *self, Entity entity)
{
	struct Bomberman bomber = {
		.entity = entity,
		.x = 0,
		.y = 0,
		.walk_dir = -1,
		.last_update = 0,
		.bomb_radius = 2,
		.dropped_bomb = {
			.entity = 0,
			.collided = false
		},
		.bombs = g_hash_table_new(NULL, NULL)
	};
	mapped_pool_set(self->data, entity, &bomber);
}

static void
sys_destroy_component(struct System *self, Entity entity)
{
	struct Bomberman *b = mapped_pool_get(self->data, entity);
	GHashTableIter i;
	gpointer key, value;

	g_hash_table_iter_init(&i, b->bombs);
	while (g_hash_table_iter_next(&i, &key, &value)) {
		game_entity_destroy(GPOINTER_TO_UINT(key));
	}

	mapped_pool_pop(self->data, entity);
}

static void*
sys_get_component(struct System *self, Entity entity)
{
	return mapped_pool_get(self->data, entity);
}

Entity
bomberman_create(Entity battlefield)
{
	// create the entity
	const ComponentType components[] = {
		BOMBERMAN, SPRITE, BODY, INPUT, 0
	};
	Entity e = game_entity_create(components);

	// initialize sprite
	sprite_init(e, "data/bomberman.png");
	sprite_zorder_set(e, 1);

	// add animations
	int frameset[] = {
		// down
		2,  0,  16, 24,
		20, 0,  16, 24,
		38, 0,  16, 24,

		// up
		59, 0,  16, 24,
		77, 0,  16, 24,
		96, 0,  16, 24,

		// left
		3,  27, 14, 23,
		21, 27, 15, 23,
		40, 27, 15, 23,

		// right
		59, 27, 15, 23,
		78, 27, 15, 23,
		97, 27, 14, 23,

		// dead
		0,  87, 18, 24,
		20, 87, 18, 24,
		40, 88, 18, 23,
		61, 88, 16, 23,
		82, 88, 23, 25
	};
	sprite_frameset_set(e, frameset, sizeof(frameset) / 4);

	{
		int frames[] = { 0, 1, 2 };
		sprite_animation_add(e, "down", frames, 3, 550);
	}
	{
		int frames[] = { 3, 4, 5 };
		sprite_animation_add(e, "up", frames, 3, 550);
	}
	{
		int frames[] = { 6, 7, 8 };
		sprite_animation_add(e, "left", frames, 3, 550);
	}
	{
		int frames[] = { 9, 10, 11 };
		sprite_animation_add(e, "right", frames, 3, 550);
	}
	{
		int frames[] = { 12, 13, 14, 15, 16 };
		sprite_animation_add(e, "dead", frames, 5, 700);
	}

	sprite_animation_play(e, "down");

	// initialize bomberman
	struct Bomberman *b = game_entity_component_get(e, BOMBERMAN);
	b->battlefield = battlefield;
	b->bombs_left = 10;
	b->x = 40;
	b->y = 36;

	// initialize the body component
	Rect body_rect = {
		.left = b->x - 11,
		.right = b->x + 11,
		.top = b->y - 11,
		.bottom = b->y + 11
	};
	body_init(e, BODY_TYPE_MOVABLE, &body_rect, BODY_BOMBERMAN);

	// start in rest state
	set_state_rest(b);

	// subscribe to events
	game_event_subscribe(
		INPUT,
		EVENT_KEYPRESS,
		e,
		handle_keys,
		GUINT_TO_POINTER(b->entity)
	);
	game_event_subscribe(
		INPUT,
		EVENT_KEYRELEASE,
		e,
		handle_keys,
		GUINT_TO_POINTER(b->entity)
	);

	// since collision events are always fired for both bodies, putting
	// this entity as event source automatically subscribes to all
	// collisions which occur with it
	game_event_subscribe(
		BODY,
		EVENT_COLLISION,
		e,
		handle_collisions,
		NULL
	);

	return e;
}

void
bomberman_system_register()
{
	struct System *sys = g_new0(struct System, 1);
	sys->name = "bomberman";
	sys->init = sys_init;
	sys->exit = sys_exit;
	sys->update = sys_update;
	sys->create_component = sys_create_component;
	sys->get_component = sys_get_component;
	sys->destroy_component = sys_destroy_component;

	game_system_register(BOMBERMAN, sys);
}
