#include "AStar.h"
#include "body.h"
#include "game/battlefield.h"
#include "game/pathfinder.h"
#include "tilemap.h"
#include <glib.h>
#include <math.h>


struct Path {
	ASPath astar_path;
	Entity map;
};


static void
node_neighbors(ASNeighborList neighbors, void *node, void *context)
{
	Entity map = ((struct Path*)context)->map;
	struct PathNode *n = node;
	struct Tile *t = tilemap_tile_at_point(map, n->x, n->y);

	static const int directions[] = {
		 0, -1,
		 1,  0,
		 0,  1,
		-1,  0
	};

	int w = rect_width(t->rect);
	int h = rect_height(t->rect);
	int x = n->x;
	int y = n->y;

	for (int i = 0; i < 4; i++) {
		int x1 = x + w * directions[i * 2];
		int y1 = y + h * directions[i * 2 + 1];
		int t = battlefield_terrain_at_point(map, x1, y1);

		// skip any non-terrain tiles
		if (t != TERRAIN_GROUND && t != TERRAIN_BLOOD)
			continue;

		// query the list of bodies which are located in current tile
		Rect r = {
			.left = x1 - 5,
			.right = x1 + 5,
			.top = y1 - 5,
			.bottom = y1 + 5
		};
		struct Body *bodies = NULL;
		size_t bodies_count = 0;
		body_get_inside_rect(r, &bodies, &bodies_count);

		// find out whether there are obstacles on path
		bool has_obstacles = false;
		while (bodies && bodies_count-- > 0) {
			struct Body b = bodies[bodies_count];
			if (b.group != BODY_TERRAIN &&
			    b.group != BODY_BOMBERMAN) {
				has_obstacles = true;
				break;
			}
		}

		if (!has_obstacles) {
			struct PathNode n1 = {
				.x = x1,
				.y = y1
			};
			ASNeighborListAdd(neighbors, &n1, 1);
		}
	}
}


static float
path_cost_heuristic(void *from, void *to, void *context)
{
	struct PathNode *a = from;
	struct PathNode *b = to;
	int cost = (abs(b->y - a->y) + abs(b->x - a->x)) / 24;
	return cost;
}


struct Path*
path_find(Entity map, int x1, int y1, int x2, int y2)
{
	ASPathNodeSource pns = {
		.nodeSize = sizeof(struct PathNode),
		.nodeNeighbors = node_neighbors,
		.pathCostHeuristic = path_cost_heuristic,
		.earlyExit = NULL,
		.nodeComparator = NULL
	};

	struct Tile *t1 = tilemap_tile_at_point(map, x1, y1);
	struct PathNode n1 = {
		.x = t1->rect.left + rect_width(t1->rect) / 2,
		.y = t1->rect.top + rect_height(t1->rect) / 2
	};

	struct Tile *t2 = tilemap_tile_at_point(map, x2, y2);
	struct PathNode n2 = {
		.x = t2->rect.left + rect_width(t2->rect) / 2,
		.y = t2->rect.top + rect_height(t2->rect) / 2
	};

	struct Path *path = g_new(struct Path, 1);
	path->map = map;
	path->astar_path = ASPathCreate(&pns, path, &n1, &n2);
	return path;
}

struct Path*
path_random(Entity map, int x1, int y1)
{
	struct Tile *t1 = tilemap_tile_at_point(map, x1, y1);
	struct PathNode n1 = {
		.x = t1->rect.left + rect_width(t1->rect) / 2,
		.y = t1->rect.top + rect_height(t1->rect) / 2
	};

	struct PathNode n2 = n1;

	switch (rand() % 4) {
	case 0: // up
		n2.y -= rect_height(t1->rect);
		break;
	case 1: // down
		n2.y += rect_height(t1->rect);
		break;
	case 2: // left
		n2.x -= rect_width(t1->rect);
		break;
	case 3: // right
		n2.x += rect_width(t1->rect);
		break;
	}

	return path_find(map, x1, y1, n2.x, n2.y);
}

size_t
path_len(struct Path *path)
{
	return ASPathGetCount(path->astar_path);
}


struct PathNode*
path_node_get(struct Path *path, size_t index)
{
	return ASPathGetNode(path->astar_path, index);
}


void
path_free(struct Path *path)
{
	ASPathDestroy(path->astar_path);
	g_free(path);
}
