#include "body.h"
#include "game/battlefield.h"
#include "sprite.h"
#include "tilemap.h"
#include <glib.h>
#include <math.h>
#include <stdlib.h>

static short field[BATTLEFIELD_ROWS * BATTLEFIELD_COLS];
extern int spawn_coords[(BOTS_COUNT + 1) * 2];


static int
random_terrain(int row, int col)
{
	if (row == 0 || row == BATTLEFIELD_ROWS - 1 ||
	    col == 0 || col == BATTLEFIELD_COLS - 1)
		return TERRAIN_WALL;

	if (col % 2 || row % 2) {
		for (int i = 0; i <= BOTS_COUNT; i++) {
			int c_col = spawn_coords[i * 2] / TILE_SIZE;
			int c_row = spawn_coords[i * 2 + 1] / TILE_SIZE;
			int dx = abs(c_col - col);
			int dy = abs(c_row - row);

			if (dx + dy <= 1)
				return TERRAIN_GROUND;
		}
		return rand() % 100 < 40 ? TERRAIN_BRICK : TERRAIN_GROUND;
	}

	return TERRAIN_WALL;
}

static void
handle_collision(struct Event *evt, void *data)
{
	struct Body *body = evt->data;
	unsigned int info = GPOINTER_TO_UINT(data);


	if (body->group == BODY_FIREBALL) {
		uint8_t col = info & 0xff;
		uint8_t row = (info >> 8) & 0xff;
		Entity tilemap = info >> 16;

		field[row * BATTLEFIELD_COLS + col] = TERRAIN_GROUND;
		tilemap_tile_update(tilemap, col, row);

		game_entity_component_remove(evt->entity, BODY);
	}
}

static void
tile_update_func(int type, struct Tile *t, void *data)
{
	size_t tile_index = t->row * BATTLEFIELD_COLS + t->col;
	int tile_type = field[tile_index];

	char *name;
	switch (tile_type) {
	case TERRAIN_GROUND:
		name = "ground";
		break;
	case TERRAIN_WALL:
		name = "wall";
		break;
	case TERRAIN_BRICK:
		name = "brick";
		break;
	case TERRAIN_BLOOD:
		name = "blood";
	}

	if (type == TILE_CREATE) {
		const ComponentType components[] = { SPRITE, 0 };
		Entity e = game_entity_create(components);
		t->data = GUINT_TO_POINTER(e);

		sprite_clone(t->tilemap, e);
		sprite_visibility_set(e, true);
		sprite_animation_play(e, name);
		sprite_position_set(
			e,
			t->rect.left + rect_width(t->rect) / 2,
			t->rect.top + rect_height(t->rect) / 2
		);

		// for brick and wall tiles add also a body component
		if (tile_type == TERRAIN_WALL || tile_type == TERRAIN_BRICK) {
			game_entity_component_add(e, BODY);
			body_init(e, BODY_TYPE_STATIC, &t->rect, BODY_TERRAIN);

			// pack the tile coordinates (1 byte each) and tilemap
			// entity into an integer, so it can be retrieved back
			// by the collision handler
			uint32_t info = t->col;
			info |= t->row << 8;
			info |= t->tilemap << 16;

			game_event_subscribe(
				BODY,
				EVENT_COLLISION,
				e,
				handle_collision,
				GUINT_TO_POINTER(info)
			);
		}
	}
	else if (type == TILE_UPDATE) {
		int x = t->rect.left + rect_width(t->rect) / 2;
		int y = t->rect.top + rect_height(t->rect) / 2;
		Entity e = GPOINTER_TO_UINT(t->data);

		sprite_animation_play(e, name);
		sprite_position_set(e, x, y);

		if (tile_type == TERRAIN_WALL || tile_type == TERRAIN_BRICK)
			body_set_position(e, x, y);
	}
	else if (type == TILE_DESTROY) {
		Entity e = GPOINTER_TO_UINT(t->data);
		game_entity_destroy(e);
	}
}

Entity
battlefield_create()
{
	const ComponentType components[] = { TILEMAP, SPRITE, 0 };
	Entity battlefield = game_entity_create(components);

	// initialize the tilemap sprite, which will act as prototype for all
	// tiles
	sprite_init(battlefield, "data/terrain.png");
	sprite_visibility_set(battlefield, false);

	// define the frameset
	int frameset[] = {
		// wall
		0,  0, 24, 24,
		// ground
		48, 0, 24, 24,
		// blood
		96, 0, 24, 24,
		// brick
		168, 0, 24, 24
	};
	sprite_frameset_set(battlefield, frameset, 4);

	// specify animations
	int f1 = 0, f2 = 1, f3 = 2, f4 = 3;
	sprite_animation_add(battlefield, "wall", &f1, 1, 0);
	sprite_animation_add(battlefield, "ground", &f2, 1, 0);
	sprite_animation_add(battlefield, "blood", &f3, 1, 0);
	sprite_animation_add(battlefield, "brick", &f4, 1, 0);

	// initialize field
	for (int row = 0; row < BATTLEFIELD_ROWS; row++) {
		for (int col = 0; col < BATTLEFIELD_COLS; col++) {
			field[row * BATTLEFIELD_COLS + col] = random_terrain(
				row, col
			);
		}
	}

	// initialize tilemap
	tilemap_init(
		battlefield,
		TILE_SIZE,
		TILE_SIZE,
		BATTLEFIELD_COLS,
		BATTLEFIELD_ROWS,
		tile_update_func,
		NULL
	);
	tilemap_position_set(battlefield, 4, 0);

	return battlefield;
}

int
battlefield_terrain_at_point(Entity battlefield, int x, int y)
{
	struct Tile *t = tilemap_tile_at_point(battlefield, x, y);
	return field[t->row * BATTLEFIELD_COLS + t->col];
}

void
battlefield_set_terrain_at_point(Entity battlefield, int type, int x, int y)
{
	struct Tile *t = tilemap_tile_at_point(battlefield, x, y);
	field[t->row * BATTLEFIELD_COLS + t->col] = type;
	tilemap_tile_update(battlefield, t->col, t->row);
}
