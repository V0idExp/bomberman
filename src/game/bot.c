#include "body.h"
#include "common.h"
#include "game/battlefield.h"
#include "game/bomberman.h"
#include "game/bot.h"
#include "game/pathfinder.h"
#include "mapped_pool.h"
#include "sprite.h"
#include "tilemap.h"
#include "timer.h"
#include "util.h"
#include <SDL_timer.h>
#include <glib.h>
#include <stdbool.h>


enum {
	STATE_ZOMBIE,
	STATE_LOOK,
	STATE_WALK
};

enum {
	WALK_DOWN,
	WALK_LEFT,
	WALK_RIGHT,
	WALK_UP
};

const char* bot_anim_names[] = {
	"0-down",
	"0-left",
	"0-right",
	"0-up",

	"1-down",
	"1-left",
	"1-right",
	"1-up",

	"2-down",
	"2-left",
	"2-right",
	"2-up"
};

int bot_anim_durations[] = {
	550,
	350,
	250
};

int bot_speeds[] = {
	// purple bug
	20,
	// blue bug
	35,
	// green bug
	55
};


struct Bot {
	Entity entity;

	// tilemap on which the bot walks
	Entity map;

	// bot type
	int type;

	// current bot state
	int state;

	// position and movement delta
	int x;
	int y;
	int dx;
	int dy;

	// path info
	struct Path *path;
	int current_node;
	int current_walk_dir;

	// last update timestamp
	Uint32 last_update;
};


static struct {
	int x;
	int y;
} player_info;


static const ComponentType bot_components[] = { BOT, SPRITE, BODY, TIMER, 0 };

// bot system data
static MappedPool *bots = 0;
static Entity bot_sprite_proto;


static void
handle_bomberman_position_change(struct Event *evt, void *data)
{
	struct PathNode *position = evt->data;

	player_info.x = position->x;
	player_info.y = position->y;
}

static void
handle_death_timeout(struct Event *evt, void *data)
{
	game_entity_component_remove(evt->entity, TIMER);
	game_event_push(BOT, EVENT_BOT_KILLED, evt->entity, NULL, 0);
}

static void
handle_collision(struct Event *evt, void *data)
{
	struct Body *body = evt->data;
	if (body->group == BODY_FIREBALL) {
		// put the bot in zombie state, so it won't get updated
		struct Bot *bot = game_entity_component_get(evt->entity, BOT);
		bot->state = STATE_ZOMBIE;

		// remove the body and the sprite, so it disappears from the
		// battlefield and does not generate collision events
		game_entity_component_remove(evt->entity, BODY);
		game_entity_component_remove(evt->entity, SPRITE);

		// splash some blood on the tile at which the bot died
		battlefield_set_terrain_at_point(
			bot->map, TERRAIN_BLOOD, bot->x, bot->y
		);

		// fire the timer which fire the bot kill event
		timer_start(evt->entity, 1000);
		game_event_subscribe(
			TIMER,
			EVENT_ALARM,
			evt->entity,
			handle_death_timeout,
			NULL
		);
	}
}


static void
set_state_look(struct Bot *b)
{
	b->state = STATE_LOOK;
	b->dx = b->dy = 0;

	sprite_position_set(b->entity, b->x, b->y);
	sprite_animation_pause(b->entity);

	// reset path
	b->current_node = 0;
	if (b->path) {
		path_free(b->path);
		b->path = NULL;
	}
}


static void
set_state_walk(struct Bot *b, struct Path *path)
{
	b->state = STATE_WALK;
	b->path = path;
	b->last_update = 0;
};


static bool
walk_to_next_node(struct Bot *b)
{
	bool node_reached = false;

	// update bot and sprite position and reset movement deltas
	b->x += b->dx;
	b->y += b->dy;
	b->dx = b->dy = 0;
	sprite_position_set(b->entity, b->x, b->y);

	struct PathNode *node = path_node_get(b->path, b->current_node);
	if (!node)
		return true;

	// if current node was reached, switch to next one and update
	// animation; this is also the starting condition
	if (b->x == node->x && b->y == node->y) {
		// skip the first node
		node_reached = b->current_node > 0;

		if (++(b->current_node) < path_len(b->path)) {
			struct PathNode *next_node = path_node_get(
				b->path, b->current_node
			);
			int dir;

			if (next_node->x == node->x)
				dir = next_node->y < node->y ? WALK_UP : WALK_DOWN;
			else
				dir = next_node->x < node->x ? WALK_LEFT : WALK_RIGHT;

			if (dir != b->current_walk_dir) {
				const char *anim = bot_anim_names[
					b->type * 4 + dir
				];
				b->current_walk_dir = dir;
				sprite_animation_play(b->entity, anim);
			}
			else if (sprite_animation_is_paused(b->entity)) {
				sprite_animation_resume(b->entity);
			}
			node = next_node;
		}

	}

	// compute time delta between current timestamp and last
	// bomberman update
	Uint32 ts = SDL_GetTicks();
	if (!b->last_update)
		b->last_update = ts;
	Uint32 delta_t = ts - b->last_update;

	// compute pixel time as milliseconds per pixel and update
	// bomberman's position delta based on walking direction
	float pixel_time = 1000.0f / bot_speeds[b->type];

	// compute the movement delta in pixels by dividing the time delta by
	// pixel time
	int delta = delta_t / pixel_time;

	// consume movement delta 1px at time, so that we don't miss the
	// destination
	while (delta > 0) {
		b->last_update = ts;

		switch (b->current_walk_dir) {
		case WALK_UP:
			b->dy--;
			break;
		case WALK_DOWN:
			b->dy++;
			break;
		case WALK_LEFT:
			b->dx--;
			break;
		case WALK_RIGHT:
			b->dx++;
		}

		if (b->x + b->dx == node->x && b->y + b->dy == node->y)
			break;

		delta--;
	}

	// move the body, effectively probing the way: if a collision between
	// this and next update occurs, the real bomberman and sprite
	// coordinates won't be updated
	body_set_position(b->entity, b->x + b->dx, b->y + b->dy);

	return node_reached;

}

static void
init_bot_sprite_prototype()
{
	const ComponentType components[] = { SPRITE, 0 };
	Entity e = game_entity_create(components);

	// initialize prototype sprite
	sprite_init(e, "data/bugs.png");
	sprite_zorder_set(e, 1);
	sprite_visibility_set(e, false);

	// add animations
	int frameset[] = {
		/* PURPLE BUG */
		// down
		9,   7, 13, 19,
		40,  6, 13, 19,
		73,  7, 13, 19,

		// left
		7,  43, 19, 13,
		40, 43, 19, 13,
		71, 43, 19, 13,

		// right
		7,  73, 19, 13,
		38, 73, 19, 13,
		71, 73, 19, 13,

		// up
		9, 102, 13, 19,
		40,103, 13, 19,
		73,102, 13, 19,

		/* BLUE BUG */
		// down
		104,  6, 15, 15,
		136,  6, 15, 15,
		168,  6, 15, 15,

		// left
		103, 40, 15, 15,
		135, 40, 15, 15,
		167, 40, 15, 15,

		// right
		106, 72, 15, 15,
		138, 72, 15, 15,
		170, 72, 15, 15,

		// up
		105, 107, 15, 15,
		137, 107, 15, 15,
		169, 107, 15, 15,

		/* GREEN BUG */
		// down
		201, 8, 15, 15,
		231, 8, 19, 15,
		264, 8, 17, 15,

		// left
		200, 40, 15, 15,
		233, 38, 15, 19,
		265, 39, 15, 17,

		// right
		200, 72, 15, 17,
		232, 71, 15, 19,
		265, 73, 15, 15,

		// up
		201, 105, 15, 15,
		231, 105, 19, 15,
		264, 105, 17, 15
	};

	int frames_count = sizeof(frameset) / sizeof(int) / 4;
	int bot_types_count = frames_count / 4 / 3;
	sprite_frameset_set(e, frameset, frames_count);

	for (int i = 0; i < bot_types_count; i++) {
		for (int j = 0; j < 4; j++) {
			int frame_offset = i * 12 + j * 3;
			int frames[] = {
				frame_offset,
				frame_offset + 1,
				frame_offset + 2
			};
			sprite_animation_add(
				e, bot_anim_names[i * 4 + j],
				frames,
				3,
				bot_anim_durations[i]
			);
		}
	}

	bot_sprite_proto = e;
}


static void
bot_system_init(struct System *self)
{
	const ComponentType deps[] = { SPRITE, TILEMAP, 0 };
	assert_game_has_systems(deps);
	bots = mapped_pool_new(sizeof(struct Bot), 20, 5);
	init_bot_sprite_prototype();

	// on bomberman's position update event, update the coordinates which
	// bots use as their walk path destination waypoint
	game_event_subscribe(
		BOMBERMAN,
		EVENT_BOMBERMAN_MOVED,
		ENTITY_ANY,
		handle_bomberman_position_change,
		NULL
	);
}


static void
bot_system_update(struct System *self)
{
	PoolIter i;
	pool_iter_init(mapped_pool_storage_get(bots), &i);

	while (pool_iter_next(&i)) {
		struct Bot *b = pool_iter_get(&i);

		// skip not initialized bots
		if (b->state == STATE_ZOMBIE) {
			continue;
		}
		// look for an update of the player position and try to compute
		// a path to it
		else if (b->state == STATE_LOOK ) {
			struct Path *path = path_find(
				b->map,
				b->x,
				b->y,
				player_info.x,
				player_info.y
			);

			// if no direct path to player was found, try with a
			// random one
			if (!path_len(path)) {
				path_free(path);
				path = path_random(b->map, b->x, b->y);
			}

			if (path_len(path))
				set_state_walk(b, path);
			else
				path_free(path);
		}
		// follow the path one node at time
		else if (b->state == STATE_WALK && walk_to_next_node(b)) {
			set_state_look(b);
		}
	}
}


static void
bot_system_exit(struct System *self)
{
	mapped_pool_free(bots);
}


static void
bot_system_create_component(struct System *self, Entity e)
{
	struct Bot b = {
		.entity = e,
		.state = STATE_ZOMBIE,
		.x = 0,
		.y = 0,
		.dx = 0,
		.dy = 0,
		.path = NULL,
		.current_node = 0,
		.current_walk_dir = -1,
		.last_update = 0
	};
	mapped_pool_set(bots, e, &b);
}


static void
bot_system_destroy_component(struct System *self, Entity e)
{
	mapped_pool_pop(bots, e);
}


static void*
bot_system_get_component(struct System *self, Entity e)
{
	return mapped_pool_get(bots, e);
}


void
bot_system_register()
{
	struct System *sys = g_new0(struct System, 1);
	sys->name = "bot";
	sys->init = bot_system_init;
	sys->exit = bot_system_exit;
	sys->update = bot_system_update;
	sys->destroy_component = bot_system_destroy_component;
	sys->create_component = bot_system_create_component;
	sys->get_component = bot_system_get_component;

	game_system_register(BOT, sys);
}


Entity
bot_create(int type, Entity map, int x, int y)
{

	Entity e = game_entity_create(bot_components);

	// compute snapped coordinates at which the bot will be positioned
	struct Tile *tile = tilemap_tile_at_point(map, x, y);
	x = tile->rect.left + rect_width(tile->rect) / 2;
	y = tile->rect.top + rect_height(tile->rect) / 2;

	// clone the sprite prototype
	sprite_clone(bot_sprite_proto, e);
	sprite_visibility_set(e, true);
	sprite_position_set(e, x, y);

	// start with paused "down" animation
	sprite_animation_play(e, bot_anim_names[type * 4 + WALK_DOWN]);

	// initialize the bot
	struct Bot *bot = game_entity_component_get(e, BOT);
	bot->type = type;
	bot->map = map;
	bot->x = x;
	bot->y = y;
	set_state_look(bot);

	// initialize bot's body
	Rect body_rect = {
		.left = x - 8,
		.right = x + 8,
		.top = y - 8,
		.bottom = y + 8
	};
	body_init(e, BODY_TYPE_MOVABLE, &body_rect, BODY_BOT);

	// subscribe the bot to collision events
	game_event_subscribe(
		BODY,
		EVENT_COLLISION,
		e,
		handle_collision,
		NULL
	);

	return e;
}
