#include "body.h"
#include "game/battlefield.h"
#include "game/bomberman.h"
#include "mapped_pool.h"
#include "sprite.h"
#include "tilemap.h"
#include "timer.h"
#include "util.h"
#include <glib.h>
#include <stdbool.h>

static const ComponentType bomb_components[] = {
	SPRITE, BOMB, BODY, TIMER, 0
};

struct Bomb {
	Entity entity;

	// the player which dropped the component
	Entity owner;

	// fireball radius in each direction
	int radius;

	// array containing fireball entity identifiers, initialized at bomb
	// drop and populated at fireball; used to clean-up fireball entities
	// at destroy
	Entity fireballs[BATTLEFIELD_ROWS + BATTLEFIELD_COLS];

	int fireball_counter;

	bool exploded;
};

struct BombSystem {
	Entity battlefield;
	Entity bomb_sprite_proto;
	Entity fireball_sprite_proto;
	GHashTable *animations_map;
	MappedPool *bombs;
};

static gpointer
make_key(char fx, char fy, bool terminal)
{
	int32_t k = fx;
	k |= fy << 8;
	k |= terminal << 16;
	return GINT_TO_POINTER(k);
}

static void
init_fireball_sprite_prototype(struct BombSystem *sys)
{
	const ComponentType components[] = { SPRITE, 0 };
	int spr = game_entity_create(components);

	sprite_init(spr, "data/fireballs.png");
	sprite_visibility_set(spr, false);

	int frameset[] = {
		// up / down
		24, 0,  24, 24,
		24, 24, 24, 24,
		24, 48, 24, 24,
		24, 72, 24, 24,

		// left / right
		0,  0,  24, 24,
		0,  24, 24, 24,
		0,  48, 24, 24,
		0,  72, 24, 24,

		// middle
		48, 0,  24, 24,
		48, 24, 24, 24,
		48, 48, 24, 24,
		48, 72, 24, 24
	};
	sprite_frameset_set(spr, frameset, 12);

	int ud_fireball[] = { 0, 1, 2, 3, 3, 2, 1, 0 };
	sprite_animation_add(spr, "ud_fireball", ud_fireball, 8, 650);

	int lr_fireball[] = { 4, 5, 6, 7, 7, 6, 5, 4 };
	sprite_animation_add(spr, "lr_fireball", lr_fireball, 8, 650);

	int md_fireball[] = { 8, 9, 10, 11, 11, 10, 9, 8 };
	sprite_animation_add(spr, "md_fireball", md_fireball, 8, 650);

	sys->fireball_sprite_proto = spr;

	// init animations map
	sys->animations_map = g_hash_table_new(NULL, NULL);
	g_hash_table_insert(
		sys->animations_map,
		make_key(0, -1, false),
		"ud_fireball"
	);
	g_hash_table_insert(
		sys->animations_map,
		make_key(0, 1, false),
		"ud_fireball"
	);
	g_hash_table_insert(
		sys->animations_map,
		make_key(-1, 0, false),
		"lr_fireball"
	);
	g_hash_table_insert(
		sys->animations_map,
		make_key(1, 0, false),
		"lr_fireball"
	);
	g_hash_table_insert(
		sys->animations_map,
		make_key(0, 0, false),
		"md_fireball"
	);
}

static void
init_bomb_sprite_prototype(struct BombSystem *sys)
{
	const ComponentType components[] = { SPRITE, 0 };

	Entity spr = game_entity_create(components);
	sprite_init(spr, "data/bomb.png");

	int frameset[] = {
		0,  0, 10, 17,
		13, 0, 10, 17,
		27, 0, 13, 17
	};
	sprite_frameset_set(spr, frameset, 3);

	int frames[] = { 0, 1, 2 };
	sprite_animation_add(spr, "default", frames, 3, 800);
	sprite_visibility_set(spr, false);

	sys->bomb_sprite_proto = spr;
}

static void
init(struct System *self)
{
	assert_game_has_systems(bomb_components);

	struct BombSystem *sys = g_new0(struct BombSystem, 1);
	sys->bombs = mapped_pool_new(
		sizeof(struct Bomb),
		BATTLEFIELD_ROWS * BATTLEFIELD_COLS / 2,
		10
	);

	init_bomb_sprite_prototype(sys);
	init_fireball_sprite_prototype(sys);

	self->data = sys;
}

static void
exit(struct System *self)
{
	struct BombSystem *sys = self->data;
	mapped_pool_free(sys->bombs);
	g_hash_table_destroy(sys->animations_map);
}

static void
create_component(struct System *self, Entity e)
{
	struct BombSystem *sys = self->data;
	struct Bomb b = {
		.entity = e,
		.owner = 0,
		.fireballs = { 0 },
		.fireball_counter = 0
	};

	mapped_pool_set(sys->bombs, e, &b);
}

static void*
get_component(struct System *self, Entity e)
{
	struct BombSystem *sys = self->data;
	return mapped_pool_get(sys->bombs, e);
}

static void
destroy_component(struct System *self, Entity e)
{
	struct BombSystem *sys = self->data;

	struct Bomb *b = mapped_pool_get(sys->bombs, e);
	for (int i = 0; i < b->fireball_counter; i++) {
		game_entity_destroy(b->fireballs[i]);
	}

	mapped_pool_pop(sys->bombs, e);
}

static void
destroy(struct Event *evt, void *data)
{
	Entity e = GPOINTER_TO_UINT(data);
	game_entity_destroy(e);
}

static void
create_fireballs(struct Bomb *b, char fx, char fy)
{
	struct BombSystem *sys = game_system_get(BOMB)->data;

	int x, y;
	sprite_position_get(b->entity, &x, &y);

	int w = tilemap_tile_width_get(sys->battlefield);
	int h = tilemap_tile_height_get(sys->battlefield);

	const ComponentType components[] = { SPRITE, BODY, 0 };

	for (int i = 1, stop = 0; i <= b->radius && stop != 1; i++) {
		int x1 = x + i * fx * w;
		int y1 = y + i * fy * h;

		int t = battlefield_terrain_at_point(sys->battlefield, x1, y1);
		if (t == TERRAIN_WALL)
			break;
		else if (t == TERRAIN_BRICK)
			stop = 1;

		Entity e = game_entity_create(components);

		// init the fireball sprite
		b->fireballs[b->fireball_counter] = e;
		b->fireball_counter++;

		sprite_clone(sys->fireball_sprite_proto, e);
		sprite_visibility_set(e, true);
		sprite_position_set(e, x1, y1);

		char *anim = g_hash_table_lookup(
			sys->animations_map, make_key(fx, fy, false)
		);
		sprite_animation_play(e, anim);

		// init the body with a rect slightly smaller than a tile
		Rect body_rect = {
			.left = x1 - 11,
			.right = x1 + 11,
			.top = y1 - 11,
			.bottom = y1 + 11
		};
		body_init(e, BODY_TYPE_MOVABLE, &body_rect, BODY_FIREBALL);

		// in case of central fireball, create just one
		if (!fx && !fy) {
			break;
		}
	}
}

static void
explode(struct Bomb *b)
{
	b->exploded = true;

	game_event_push(BOMB, EVENT_BOMB_EXPLOSION, b->entity, NULL, 0);
	game_entity_component_remove(b->entity, BODY);

	// make the bomb sprite invisible
	sprite_visibility_set(b->entity, false);

	// create fireballs in all directions
	create_fireballs(b,  0, -1);
	create_fireballs(b,  0,  1);
	create_fireballs(b,  1,  0);
	create_fireballs(b, -1,  0);

	// create central fireball
	create_fireballs(b,  0,  0);

	// when the animation of the fireball in the middle finishes, destroy
	// the bomb
	Entity md_fireball = b->fireballs[b->fireball_counter - 1];
	game_event_subscribe(
		SPRITE,
		EVENT_ANIMATION_FINISHED,
		md_fireball,
		destroy,
		GUINT_TO_POINTER(b->entity)
	);
}

void
handle_timeout(struct Event *evt, void *data)
{
	struct Bomb *b = game_entity_component_get(evt->entity, BOMB);
	if (!b->exploded)
		explode(b);
}

void
handle_collision(struct Event *evt, void *data)
{
	struct Bomb *b = game_entity_component_get(evt->entity, BOMB);
	struct Body *body = evt->data;
	if (!b->exploded && body->group == BODY_FIREBALL) {
		explode(b);
	}
}

void
bomb_system_register()
{
	struct System *sys = g_new0(struct System, 1);
	sys->name = "bomb";
	sys->init = init;
	sys->exit = exit;
	sys->create_component = create_component;
	sys->get_component = get_component;
	sys->destroy_component = destroy_component;
	game_system_register(BOMB, sys);
}

void
bomb_system_init(Entity battlefield)
{
	struct BombSystem *sys = game_system_get(BOMB)->data;
	sys->battlefield = battlefield;
}

Entity
bomb_drop(Entity owner, int x, int y, int radius, int timer)
{
	struct BombSystem *sys = game_system_get(BOMB)->data;
	g_assert(sys->battlefield != 0);

	Entity e = game_entity_create(bomb_components);

	struct Bomb *bomb = game_entity_component_get(e, BOMB);
	bomb->radius = radius;

	sprite_clone(sys->bomb_sprite_proto, e);
	sprite_position_set(e, x, y);
	sprite_visibility_set(e, true);
	sprite_animation_play(e, "default");

	Rect bomb_rect = {
		.left = x - 12,
		.right = x + 12,
		.top = y - 12,
		.bottom = y + 12
	};
	body_init(e, BODY_TYPE_STATIC, &bomb_rect, BODY_BOMB);

	game_event_subscribe(
		TIMER,
		EVENT_ALARM,
		e,
		handle_timeout,
		NULL
	);

	game_event_subscribe(
		BODY,
		EVENT_COLLISION,
		e,
		handle_collision,
		NULL
	);

	timer_start(e, timer);
	return e;
}
