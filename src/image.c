#include "common.h"
#include "image.h"
#include <SDL_image.h>
#include <SDL_video.h>
#include <glib.h>
#include <stdio.h>
#include <stdlib.h>

static void
image_system_init(struct System *self)
{
	int init_formats = IMG_INIT_PNG;
	if (IMG_Init(init_formats) != init_formats) {
		fprintf(stderr, "failed to initialize SDL_image\n");
		exit(EXIT_FAILURE);
	}
}

static void
image_system_exit(struct System *self)
{
	IMG_Quit();
}

struct SDL_Surface*
image_load(struct System *sys, const char *filename)
{
	SDL_Surface *img = IMG_Load(filename);
	if (!img) {
		fprintf(
			stderr,
			"failed to load image '%s'\n%s",
			filename,
			IMG_GetError()
		);
		exit(EXIT_FAILURE);
	}

	printf("loaded image '%s'\n", filename);
	return img;
}

void
image_system_register()
{
	struct System *sys = g_new0(struct System, 1);
	sys->name = "image";
	sys->init = image_system_init;
	sys->exit = image_system_exit;
	game_system_register(IMAGE, sys);
}
