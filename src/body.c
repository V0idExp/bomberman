#include "body.h"
#include "mapped_pool.h"
#include <chipmunk/chipmunk.h>
#include <glib.h>
#include <stddef.h>
#include <string.h>

struct BodyInfo {
	struct Body body;

	/* chipmunk stuff */
	cpBody *cp_body;
	cpShape *cp_shape;
};

struct BodySystem {
	MappedPool *bodies;

	/* chipmunk stuff */
	cpSpace *space;
};

static cpBool
collision_handler(cpArbiter *arb, cpSpace *space, cpDataPointer data)
{
	if (!cpArbiterIsFirstContact(arb))
		return 0;

	cpShape *a, *b;
	cpArbiterGetShapes(arb, &a, &b);

	struct Body *first = cpShapeGetUserData(a);
	struct Body *second = cpShapeGetUserData(b);

	game_event_push(
		BODY,
		EVENT_COLLISION,
		first->entity,
		second,
		0 // don't free the event after dispatching
	);

	game_event_push(
		BODY,
		EVENT_COLLISION,
		second->entity,
		first,
		0 // don't free the event after dispatching
	);

	// we're not interested in additional collision processing, AABB-level
	// is fine for our needs
	return 0;
}

static void
sys_init(struct System *self)
{
	struct BodySystem *sys = g_new0(struct BodySystem, 1);
	self->data = sys;

	// initialize system
	sys->bodies = mapped_pool_new(sizeof(struct BodyInfo), 1000, 250);

	// initialize chipmunk
	sys->space = cpSpaceNew();

	// initialize collision handlers
	cpCollisionHandler *handler = cpSpaceAddDefaultCollisionHandler(sys->space);
	handler->beginFunc = collision_handler;
}

static void
sys_exit(struct System *self)
{
	struct BodySystem *sys = self->data;

	cpSpaceFree(sys->space);
	mapped_pool_free(sys->bodies);
	g_free(sys);
}

static void
sys_update(struct System *self)
{
	struct BodySystem *sys = self->data;
	cpFloat time_step = 1.0 / 60.0;
	cpSpaceStep(sys->space, time_step);
}

static void
sys_create_component(struct System *self, Entity entity)
{
	struct BodySystem *sys = self->data;
	struct BodyInfo info = {
		.body.entity = entity,
		.cp_body = NULL,
		.cp_shape = NULL
	};
	mapped_pool_set(sys->bodies, entity, &info);
}

static void
sys_destroy_component(struct System *self, Entity entity)
{
	struct BodySystem *sys = self->data;
	struct BodyInfo *b = mapped_pool_get(sys->bodies, entity);

	cpSpaceRemoveShape(sys->space, b->cp_shape);
	cpShapeDestroy(b->cp_shape);
	cpBodyDestroy(b->cp_body);

	mapped_pool_pop(sys->bodies, entity);
}

static void*
sys_get_component(struct System *self, Entity entity)
{
	struct BodySystem *sys = self->data;
	struct BodyInfo *b = mapped_pool_get(sys->bodies, entity);

	return &b->body;
}

void
body_system_register()
{
	struct System *sys = g_new0(struct System, 1);

	sys->name = "body";
	sys->init = sys_init;
	sys->exit = sys_exit;
	sys->update = sys_update;
	sys->create_component = sys_create_component;
	sys->destroy_component = sys_destroy_component;
	sys->get_component = sys_get_component;

	game_system_register(BODY, sys);
}

void
body_init(Entity e, BodyType t, Rect *r, unsigned int group)
{
	g_assert_nonnull(r);
	g_assert_true(r->left < r->right);
	g_assert_true(r->top < r->bottom);

	struct BodySystem *sys = game_system_get(BODY)->data;
	struct BodyInfo *b = mapped_pool_get(sys->bodies, e);

	// try to initialize the body or fail if already initialized
	if (b->cp_body)
		game_log(LOG_FATAL, "body %d is already initialized\n", e);

	b->body.type = t;
	b->body.rect = *r;
	b->body.group = group;

	int w = rect_ptr_width(r);
	int h = rect_ptr_height(r);
	int x = r->left + w / 2;
	int y = r->top + h / 2;

	// statics are attached to a special static body of chipmunk
	// space, movables have a new cpBodyInfo created for each one
	switch (t) {
	case BODY_TYPE_STATIC:
		b->cp_body = cpSpaceGetStaticBody(sys->space);

		// box shape is specified explicitly with offset, because we
		// have one body for all statics
		b->cp_shape = cpBoxShapeNew2(
			b->cp_body,
			cpBBNew(r->left, r->top, r->right, r->bottom),
			0
		);
		break;

	// movables have their own body and shape
	case BODY_TYPE_MOVABLE:
		// the body is not attached to space, mass is not relevant and
		// is set to 1, as well as moment of inertia
		b->cp_body = cpBodyNew(1, 1);

		// the shape is centered around the center of gravity
		b->cp_shape = cpBoxShapeNew(b->cp_body, w, h, 0);

		cpBodySetPosition(b->cp_body, cpv(x, y));
		break;

	default:
		game_log(LOG_FATAL, "invalid body type %d\n", t);
	}

	// set the filter for the shape
	cpShapeFilter filter = cpShapeGetFilter(b->cp_shape);
	filter.group = group;
	cpShapeSetFilter(b->cp_shape, filter);

	// set the collision type
	cpShapeSetCollisionType(b->cp_shape, t);

	// store the Body as userdata
	cpShapeSetUserData(b->cp_shape, &b->body);

	// add the shape to the space; after this call collisions with it will
	// be computed and reported
	cpSpaceAddShape(sys->space, b->cp_shape);
}

void
body_set_position(Entity entity, int x, int y)
{
	struct BodySystem *sys = game_system_get(BODY)->data;
	struct BodyInfo *b = mapped_pool_get(sys->bodies, entity);

	if (!b->cp_body) {
		game_log(LOG_FATAL, "body %d is not initialized\n", entity);
		exit(EXIT_FAILURE);
	}

	// update the rect position
	Rect *r = &b->body.rect;
	int w = rect_width(b->body.rect);
	int h = rect_height(b->body.rect);
	r->left = x - w / 2;
	r->right = r->left + w;
	r->top = y - h / 2;
	r->bottom = r->top + h;

	if (b->body.type == BODY_TYPE_MOVABLE) {
		cpBodySetPosition(b->cp_body, cpv(x, y));
	}
	else {
		// destroy existing shape
		cpSpaceRemoveShape(sys->space, b->cp_shape);
		cpShapeDestroy(b->cp_shape);

		cpShapeFilter filter = cpShapeGetFilter(b->cp_shape);

		// re-create the shape with new rect
		b->cp_shape = cpBoxShapeNew2(
			b->cp_body,
			cpBBNew(r->left, r->top, r->right, r->bottom),
			0
		);

		cpShapeSetFilter(b->cp_shape, filter);
		cpShapeSetCollisionType(b->cp_shape, BODY_TYPE_STATIC);
		cpShapeSetUserData(b->cp_shape, &b->body);
		cpSpaceAddShape(sys->space, b->cp_shape);
	}
}

bool
body_get_nearest(int x, int y, struct Body *dst)
{
	struct BodySystem *sys = game_system_get(BODY)->data;

	cpShapeFilter filter = { 0 };
	filter.mask = filter.categories = ~(filter.mask & 0);

	cpShape *shape = cpSpacePointQueryNearest(
		sys->space,
		cpv(x, y),
		0,
		filter,
		NULL
	);

	if (shape && dst) {
		memcpy(
			dst, cpShapeGetUserData(shape), sizeof(struct Body)
		);
		return true;
	}

	return shape != NULL;
}

static void
query_result_iterator(cpShape *shape, void *data)
{
	GArray *dest = data;
	struct Body *b = cpShapeGetUserData(shape);

	g_array_append_val(dest, *b);
}

void
body_get_inside_rect(Rect r, struct Body **result_set, size_t *size)
{
	struct BodySystem *sys = game_system_get(BODY)->data;

	GArray *dest = g_array_new(false, false, sizeof(struct Body));

	// no filter, basically
	cpShapeFilter filter = { 0 };
	filter.mask = filter.categories = ~(filter.mask & 0);

	cpBB bb = {
		.l = r.left,
		.r = r.right,
		.t = r.top,
		.b = r.bottom
	};

	cpSpaceBBQuery(sys->space, bb, filter, query_result_iterator, dest);
	*result_set = (struct Body*)dest->data;
	*size = dest->len;

	g_array_free(dest, false);
}
