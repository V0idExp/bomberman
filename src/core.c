#include "common.h"
#include "core.h"
#include <SDL.h>
#include <glib.h>
#include <stdio.h>
#include <stdlib.h>

void
core_system_init(struct System *self)
{
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER | SDL_INIT_EVENTS) != 0) {
		fprintf(stderr, "failed to initialize SDL\n");
		exit(EXIT_FAILURE);
	}
}

void
core_system_exit(struct System *self)
{
	SDL_Quit();
}

void
core_system_register()
{
	struct System *sys = g_new0(struct System, 1);
	sys->name = "core";
	sys->init = core_system_init;
	sys->exit = core_system_exit;

	game_system_register(CORE, sys);
}

