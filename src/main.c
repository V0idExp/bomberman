#include "body.h"
#include "common.h"
#include "core.h"
#include "game/battlefield.h"
#include "game/bomberman.h"
#include "game/bot.h"
#include "game/pathfinder.h"
#include "gfx.h"
#include "image.h"
#include "input.h"
#include "sprite.h"
#include "tilemap.h"
#include "timer.h"
#include <SDL_events.h>
#include <stdlib.h>
#include <time.h>

// game states
enum {
	GAME_RUNNING,
	GAME_OVER,
	GAME_VICTORY
};

// quit on true
static bool quit = false;

// player and bots spawn coordinates, used also in battlefield.c
int spawn_coords[(BOTS_COUNT + 1) * 2] = {
	40,  35,
};

struct {
	int state;
	int bots_left;
	Entity battlefield;
	Entity bomberman;
	Entity bots[BOTS_COUNT];
	Entity splashscreen;
} game;

static void
handle_bomberman_death(struct Event *evt, void *data);

static void
handle_bot_death(struct Event *evt, void *data);

static void
set_gamestate_running()
{
	game_log(LOG_INFO, "game running");
	game.state = GAME_RUNNING;
	game.bots_left = BOTS_COUNT;

	// generate random bot spawn coordinates
	for (int i = 1; i <= BOTS_COUNT; i++) {
		int row, col;
		while (1) {
			// make sure bots are far enough from the player spawn
			// position
			col = 3 + (rand() % (BATTLEFIELD_COLS - 3));
			row = 3 + (rand() % (BATTLEFIELD_ROWS - 3));

			if (col % 2 && row % 2)
				break;
		}
		spawn_coords[i * 2] = col * TILE_SIZE;
		spawn_coords[i * 2 + 1] = row * TILE_SIZE;
	}

	// hide splash screen
	sprite_visibility_set(game.splashscreen, false);

	// create the battlefield (uses the just generated spawn coordinates to
	// position correctly the bricks, so that bots and player are not
	// completely surrounded)
	game.battlefield = battlefield_create();

	// create the bomberman and subscribe to his death
	game.bomberman = bomberman_create(game.battlefield);
	game_event_subscribe(
		BOMBERMAN,
		EVENT_BOMBERMAN_KILLED,
		game.bomberman,
		handle_bomberman_death,
		NULL
	);

	// initialize the bomb system with current battlefield
	bomb_system_init(game.battlefield);

	// create bots and subscribe to their death
	for (int i = 1; i <= BOTS_COUNT; i++ ) {
		int type = rand() % 3;
		int x = spawn_coords[i * 2];
		int y = spawn_coords[i * 2 + 1];

		Entity bot = bot_create(type, game.battlefield, x, y);

		game_event_subscribe(
			BOT,
			EVENT_BOT_KILLED,
			bot,
			handle_bot_death,
			NULL
		);

		game.bots[i - 1] = bot;
	}
}

static void
cleanup() {
	// destroy the bomberman, if he's still alive
	if (game.bomberman)
		game_entity_destroy(game.bomberman);

	// destroy remaining bots
	for (int i = 0; i < BOTS_COUNT; i++) {
		if (game.bots[i])
			game_entity_destroy(game.bots[i]);
	}

	// destroy the battlefield
	game_entity_destroy(game.battlefield);
}

static void
set_gamestate_over()
{
	game_log(LOG_INFO, "game over");
	game.state = GAME_OVER;

	// show the game over splash animation
	sprite_animation_play(game.splashscreen, "game-over");
	sprite_visibility_set(game.splashscreen, true);

	cleanup();

}

static void
set_gamestate_victory()
{
	game_log(LOG_INFO, "victory");
	game.state = GAME_VICTORY;

	// show the game over splash animation
	sprite_animation_play(game.splashscreen, "victory");
	sprite_visibility_set(game.splashscreen, true);

	cleanup();
}

static void
handle_keys(struct Event *evt, void *data)
{
	struct SDL_KeyboardEvent *e = (SDL_KeyboardEvent*)evt->data;
	switch (e->keysym.sym) {
	case SDLK_q:
	case SDLK_ESCAPE:
		quit = true;
		break;

	case SDLK_RETURN:
		if (game.state == GAME_OVER || game.state == GAME_VICTORY)
			set_gamestate_running();
		break;
	}
}

static void
handle_bomberman_death(struct Event *evt, void *data)
{
	game_log(LOG_INFO, "...RIP bomberman!");
	game_entity_destroy(evt->entity);
	game.bomberman = 0;
}

static void
handle_bot_death(struct Event *evt, void *data)
{
	game_log(LOG_INFO, "..die, bot #%d!", evt->entity);
	game_entity_destroy(evt->entity);

	// remove the bot from the list of living bots and decrement the bot
	// counter
	for (int i = 0; i < BOTS_COUNT; i++) {
		if (game.bots[i] == evt->entity) {
			game.bots[i] = 0;
			game.bots_left--;
		}
	}
}

int
main(int argc, char **argv)
{
	// initialize random number generator
	time_t t;
	srand((unsigned)time(&t));

	game_init();
	game_log_level_set(LOG_INFO);

	core_system_register();
	input_system_register();
	body_system_register();
	gfx_system_register();
	image_system_register();
	sprite_system_register();
	bomberman_system_register();
	tilemap_system_register();
	timer_system_register();
	bomb_system_register();
	bot_system_register();

	/*** ONE TIME INITIALIZATIONS ***/

	// plain keyboard event listener entity for quit / rerunning events
	{
		ComponentType components[] = { INPUT, 0 };
		Entity e = game_entity_create(components);

		game_event_subscribe(
			INPUT,
			EVENT_KEYRELEASE,
			e,
			handle_keys,
			NULL
		);
	}
	// game over splashscreen
	{
		ComponentType components[] = { SPRITE, 0 };
		Entity e = game_entity_create(components);
		int frameset[] = {
			// game over
			0,   0,   417, 131,
			417, 0,   417, 131,
			// victory
			0,   131, 417, 131,
			417, 131, 417, 131
		};
		sprite_init(e, "data/splashscreen.png");
		sprite_zorder_set(e, 3);
		sprite_frameset_set(e, frameset, 4);
		sprite_position_set(e, 400, 300);

		int game_over_frames[] = { 0, 1 };
		sprite_animation_add(e, "game-over", game_over_frames, 2, 1000);

		int victory_frames[] = { 2, 3 };
		sprite_animation_add(e, "victory", victory_frames, 2, 1000);

		game.splashscreen = e;
	}

	// start with running game state
	set_gamestate_running();

	while (!quit) {
		// check victory or defeat conditions and update the game state
		// accordingly
		if (!game.bomberman && game.state == GAME_RUNNING)
			set_gamestate_over();
		else if (!game.bots_left && game.state == GAME_RUNNING)
			set_gamestate_victory();

		// updates systems and dispatches events!
		game_update();
	}

	game_exit();

	return 0;
}
