#include "common.h"
#include "input.h"
#include <SDL.h>
#include <glib.h>

static void
input_system_init(struct System *self)
{
	self->data = g_hash_table_new(NULL, NULL);
}

static void
input_system_exit(struct System *self)
{
	g_hash_table_destroy(self->data);
}

static void
input_system_component_create(struct System *self, Entity e)
{
	g_hash_table_add(self->data, GUINT_TO_POINTER(e));
}

static void
input_system_component_destroy(struct System *self, Entity e)
{
	g_hash_table_remove(self->data, GUINT_TO_POINTER(e));
}

static void
input_system_update(struct System *self)
{
	SDL_Event e;
	while (SDL_PollEvent(&e)) {
		void *evt;
		EventType type = 0;
		size_t size;

		switch (e.type) {
		case SDL_KEYUP:
		case SDL_KEYDOWN:
			if (e.type == SDL_KEYDOWN)
				type = EVENT_KEYPRESS;
			else
				type = EVENT_KEYRELEASE;

			size = sizeof(SDL_KeyboardEvent);
			evt = &e.key;
			break;

		case SDL_MOUSEBUTTONUP:
		case SDL_MOUSEBUTTONDOWN:
			if (e.type == SDL_MOUSEBUTTONDOWN)
				type = EVENT_MOUSEPRESS;
			else
				type = EVENT_MOUSERELEASE;

			size = sizeof(SDL_MouseButtonEvent);
			evt = &e.button;
			break;
		}

		if (type == 0)
			continue;

		GHashTableIter iter;
		gpointer key, value;

		g_hash_table_iter_init(&iter, self->data);
		while (g_hash_table_iter_next(&iter, &key, &value)) {
			game_event_push(
				INPUT,
				type,
				GPOINTER_TO_UINT(key),
				evt,
				size
			);
		}
	}
}

void
input_system_register()
{
	struct System *sys = g_new0(struct System, 1);
	sys->name = "input";
	sys->init = input_system_init;
	sys->exit = input_system_exit;
	sys->create_component = input_system_component_create;
	sys->destroy_component = input_system_component_destroy;
	sys->update = input_system_update;
	game_system_register(INPUT, sys);
}
