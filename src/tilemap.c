#include "common.h"
#include "mapped_pool.h"
#include "tilemap.h"
#include <glib.h>

struct TileMap {
	Entity entity;
	TileUpdateFunc *update;
	RefCounter *user_data;
	unsigned short tile_width;
	unsigned short tile_height;
	unsigned int rows;
	unsigned int cols;
	int top;
	int left;
	struct Tile *tiles;
};

static void
tilemap_system_init(struct System *self)
{
	self->data = mapped_pool_new(sizeof(struct TileMap), 5, 1);
}

static void
tilemap_system_exit(struct System *self)
{
	mapped_pool_free(self->data);
}

static void
tilemap_system_create_component(struct System *self, Entity entity)
{
	struct TileMap map = {
		.entity = entity,
		.user_data = NULL,
		.update = NULL,
		.tile_width = 0,
		.tile_height = 0,
		.rows = 0,
		.cols = 0,
		.left = 0,
		.top = 0,
		.tiles = NULL
	};
	mapped_pool_set(self->data, entity, &map);
}

static void*
tilemap_system_get_component(struct System *self, Entity entity)
{
	return mapped_pool_get(self->data, entity);
}

static void
tilemap_system_destroy_component(struct System *self, Entity entity)
{
	struct TileMap *map = mapped_pool_get(self->data, entity);

	// notify tiles about destroy
	void *user_data_ptr = NULL;
	if (map->user_data)
		user_data_ptr = ref_counter_get(map->user_data);

	for (int row = 0; row < map->rows; row++) {
		for (int col = 0; col < map->cols; col++) {
			struct Tile *t = map->tiles + row * map->cols + col;
			map->update(TILE_DESTROY, t, user_data_ptr);
		}
	}

	if (map->user_data)
		ref_counter_dec(map->user_data);

	g_free(map->tiles);

	mapped_pool_pop(self->data, entity);
}

void
tilemap_system_register()
{
	struct System *sys = g_new0(struct System, 1);
	sys->name = "tilemap";
	sys->init = tilemap_system_init;
	sys->exit = tilemap_system_exit;
	sys->create_component = tilemap_system_create_component;
	sys->get_component = tilemap_system_get_component;
	sys->destroy_component = tilemap_system_destroy_component;

	game_system_register(TILEMAP, sys);
}

void
tilemap_init(
	Entity entity,
	unsigned short tile_width,
	unsigned short tile_height,
	unsigned int cols,
	unsigned int rows,
	TileUpdateFunc *update,
	RefCounter *user_data
) {
	g_assert_true(entity > 0);
	g_assert_nonnull(update);

	struct TileMap *tilemap = game_entity_component_get(entity, TILEMAP);

	tilemap->update = update;
	tilemap->user_data = user_data ? ref_counter_inc(user_data) : NULL;
	tilemap->tile_width = tile_width;
	tilemap->tile_height = tile_height;
	tilemap->rows = rows;
	tilemap->cols = cols;
	tilemap->tiles = g_new0(struct Tile, rows * cols);

	void *user_data_ptr = user_data ? ref_counter_get(user_data) : NULL;

	// populate map with tiles
	for (int row = 0; row < rows; row++) {
		for (int col = 0; col < cols; col++) {
			// initialize the tile
			struct Tile *t = tilemap->tiles + row * cols + col;
			t->tilemap = entity;
			t->col = col;
			t->row = row;
			t->rect.left   = col * tile_width;
			t->rect.right  = col * tile_width + tile_width;
			t->rect.top    = row * tile_height;
			t->rect.bottom = row * tile_height + tile_height;

			update(TILE_CREATE, t, user_data_ptr);
		}
	}
}

void
tilemap_position_set(Entity entity, int left, int top)
{
	struct TileMap *tilemap = game_entity_component_get(entity, TILEMAP);
	int rows = tilemap->rows, cols = tilemap->cols;

	tilemap->top = top;
	tilemap->left = left;

	void *user_data_ptr = NULL;
	if (tilemap->user_data)
		user_data_ptr = ref_counter_get(tilemap->user_data);

	for (int r = 0; r < rows; r++) {
		for (int c = 0; c < cols; c++) {
			struct Tile *t = tilemap->tiles + r * cols + c;

			// update tile rect
			t->rect.left = c * tilemap->tile_width + tilemap->left;
			t->rect.top = r * tilemap->tile_height + tilemap->top;
			t->rect.right = t->rect.left + tilemap->tile_width;
			t->rect.bottom = t->rect.top + tilemap->tile_height;

			tilemap->update(TILE_UPDATE, t, user_data_ptr);
		}
	}
}

struct Tile*
tilemap_tile_at_point(Entity entity, int x, int y)
{
	struct TileMap *tilemap = game_entity_component_get(entity, TILEMAP);

	int x0 = x - tilemap->left;
	int y0 = y - tilemap->top;

	int c = x0 / tilemap->tile_width;
	int r = y0 / tilemap->tile_height;

	if (c > tilemap->cols || r > tilemap->rows)
		return NULL;

	return tilemap->tiles + r * tilemap->cols + c;
}

unsigned int
tilemap_tile_width_get(Entity entity)
{
	struct TileMap *tilemap = game_entity_component_get(entity, TILEMAP);
	return tilemap->tile_width;
}

unsigned int
tilemap_tile_height_get(Entity entity)
{
	struct TileMap *tilemap = game_entity_component_get(entity, TILEMAP);
	return tilemap->tile_height;
}

void
tilemap_tile_update(Entity entity, int col, int row)
{
	struct TileMap *tilemap = game_entity_component_get(entity, TILEMAP);

	if (row >= tilemap->rows || col >= tilemap->cols) {
		game_log(
			LOG_FATAL,
			"tile coordinates %d,%d out of bounds\n",
			col,
			row
		);

	}

	void *user_data_ptr = NULL;
	if (tilemap->user_data)
		user_data_ptr = ref_counter_get(tilemap->user_data);

	struct Tile *t = tilemap->tiles + row * tilemap->cols + col;
	tilemap->update(TILE_UPDATE, t, user_data_ptr);
}
