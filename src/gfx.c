#include "common.h"
#include "gfx.h"
#include <SDL_timer.h>
#include <glib.h>

struct GfxContext {
	SDL_Window *window;
	SDL_Renderer *renderer;
};

static void
gfx_system_init(struct System *self)
{
	struct GfxContext *ctx = g_new(struct GfxContext, 1);

	// create window
	SDL_Window *win = SDL_CreateWindow(
		"Bomberman",
		SDL_WINDOWPOS_UNDEFINED,
		SDL_WINDOWPOS_UNDEFINED,
		800,
		600,
		0
	);
	if (!win) {
		fprintf(
			stderr,
			"failed to create SDL window:\n%s\n",
			SDL_GetError()
		);
		exit(EXIT_FAILURE);
	}

	// create renderer
	SDL_Renderer *renderer = SDL_CreateRenderer(
		win,
		-1,
		SDL_RENDERER_ACCELERATED
	);
	if (!renderer) {
		fprintf(
			stderr,
			"failed to create SDL renderer:\n%s\n",
			SDL_GetError()
		);
		exit(EXIT_FAILURE);
	}

	ctx->window = win;
	ctx->renderer = renderer;
	self->data = ctx;
}

static void
gfx_system_exit(struct System *self)
{
	struct GfxContext *ctx = self->data;
	SDL_DestroyRenderer(ctx->renderer);
	SDL_DestroyWindow(ctx->window);
	g_free(ctx);
}

static void
gfx_system_update(struct System *self)
{
	struct GfxContext *ctx = self->data;
	SDL_RenderPresent(ctx->renderer);
	SDL_RenderClear(ctx->renderer);
	SDL_Delay(10);
}

void
gfx_system_register()
{
	struct System *sys = g_new0(struct System, 1);
	sys->name = "gfx";
	sys->init = gfx_system_init;
	sys->exit = gfx_system_exit;
	sys->update = gfx_system_update;

	game_system_register(GFX, sys);
}

SDL_Window*
gfx_system_get_window(struct System *gfx_system)
{
	struct GfxContext *ctx = gfx_system->data;

	return ctx->window;
}

SDL_Renderer*
gfx_system_get_renderer(struct System *gfx_system)
{
	struct GfxContext *ctx = gfx_system->data;

	return ctx->renderer;
}
