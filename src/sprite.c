#include "common.h"
#include "gfx.h"
#include "image.h"
#include "sprite.h"
#include "util.h"
#include <SDL_render.h>
#include <SDL_timer.h>
#include <assert.h>
#include <glib.h>
#include <stdlib.h>
#include <string.h>

struct Animation {
	GArray *frames;
	int current_frame;
	unsigned int last_update;
	int duration;
	bool paused;
};

struct Sprite {
	struct System *creator;
	Entity entity;
	int x;
	int y;
	int z;
	int width;
	int height;
	bool initialized;
	bool visible;
	struct Animation *current_animation;

	// reference-counted SDL_Texture
	RefCounter *texture;

	GArray *frameset;
	GHashTable *animations;
};


struct SpriteSysData {
	struct {
		struct System *gfx;
		struct System *img;
	} systems;

	GArray *sprites;
};

static int
sprite_z_cmp(gconstpointer a, gconstpointer b)
{
	int z1 = ((struct Sprite*)a)->z;
	int z2 = ((struct Sprite*)b)->z;
	if (z1 < z2)
		return -1;
	else if (z1 == z2)
		return 0;
	return 1;
}

static struct Sprite*
get_sprite(Entity e)
{
	return game_entity_component_get(e, SPRITE);
}

static void
destroy_sdl_texture(void *tex)
{
	SDL_DestroyTexture((SDL_Texture*)tex);
}

static void
destroy_animation(void *ptr)
{
	struct Animation *anim = ptr;
	g_array_unref(anim->frames);
	g_free(anim);
}

static void
sprite_system_init(struct System *self)
{
	const ComponentType deps[] = {
		GFX, IMAGE, 0
	};
	assert_game_has_systems(deps);

	// store pointers to relevant systems in internal data structure for
	// later use and allocate array for sprites
	struct SpriteSysData *sysdata = g_new0(struct SpriteSysData, 1);
	sysdata->systems.gfx = game_system_get(GFX);
	sysdata->systems.img = game_system_get(IMAGE);
	sysdata->sprites = g_array_new(false, false, sizeof(struct Sprite));
	self->data = sysdata;
}

static void
sprite_system_exit(struct System *self)
{
	// TODO
}

static void
sprite_system_update(struct System *self)
{
	struct SpriteSysData *ssd = self->data;
	struct SDL_Renderer *rndr = gfx_system_get_renderer(ssd->systems.gfx);

	for (int i = 0; i < ssd->sprites->len; i++) {
		struct Sprite *spr = &g_array_index(
			ssd->sprites, struct Sprite, i
		);
		struct Animation *anim = spr->current_animation;

		if (spr->initialized && anim) {
			int frame_index = *(&g_array_index(
				anim->frames, int, anim->current_frame
			));

			// compute source and destination rectangles
			SDL_Rect *frame = &g_array_index(
				spr->frameset, SDL_Rect, frame_index
			);

			spr->width = frame->w;
			spr->height = frame->h;

			SDL_Rect dst = {
				.x = spr->x - frame->w / 2,
				.y = spr->y - frame->h / 2,
				.w = frame->w,
				.h = frame->h
			};

			// render frame, if visibility is enabled
			if (spr->visible) {
				int bad_render = SDL_RenderCopy(
					rndr,
					ref_counter_get(spr->texture),
					frame,
					&dst
				);
				if (bad_render) {
					game_log(
						LOG_FATAL,
						"failed to render frame: %s\n",
						SDL_GetError()
					);
				}
			}

			// skip animation playback on static sprites
			if (anim->duration == 0)
				continue;

			// compute delta between current timestamp and previous
			// update
			unsigned int ts = SDL_GetTicks();
			if (!anim->last_update)
				anim->last_update = ts;

			unsigned int delta = ts - anim->last_update;
			float t = anim->duration / (float)anim->frames->len;

			while (delta >= t) {
				anim->last_update = ts;
				delta -= t;

				// update the frame, if animation is not paused
				if (anim->paused)
					continue;
				else if (anim->current_frame < anim->frames->len - 1) {
					anim->current_frame++;
				}
				else {
					anim->current_frame = 0;
					game_event_push(
						SPRITE,
						EVENT_ANIMATION_FINISHED,
						spr->entity,
						NULL,
						0
					);
				}
			}
		}
	}
}

static void
sprite_system_create_component(struct System *self, Entity entity)
{
	struct SpriteSysData *ssd = self->data;

	// check if there's already a sprite for given entity
	for (int i = 0; i < ssd->sprites->len; i++) {
		struct Sprite *spr = NULL;
		spr = &g_array_index(ssd->sprites, struct Sprite, i);
		if (spr->entity == entity) {
			game_log(
				LOG_FATAL,
				"sprite for entity %d already exists\n",
				entity
			);
		}
	}

	struct Sprite spr = {
		.creator = self,
		.entity = entity,
		.x = 0,
		.y = 0,
		.z = 0,
		.width = 0,
		.height = 0,
		.initialized = false,
		.visible = true,
		.texture = NULL,
		.current_animation = NULL,
		.frameset = NULL,
		.animations = g_hash_table_new_full(
			g_str_hash,
			g_str_equal,
			NULL,
			destroy_animation
		)
	};

	g_array_append_val(ssd->sprites, spr);
	g_array_sort(ssd->sprites, sprite_z_cmp);
}

static void
sprite_system_destroy_component(struct System *self, Entity entity)
{
	struct SpriteSysData *ssd = self->data;

	for (int i = 0; i < ssd->sprites->len; i++) {
		struct Sprite *spr = &g_array_index(
			ssd->sprites, struct Sprite, i
		);

		if (spr->entity == entity) {
			if (spr->initialized)
				ref_counter_dec(spr->texture);

			g_array_unref(spr->frameset);
			g_hash_table_destroy(spr->animations);
			g_array_remove_index(ssd->sprites, i);
			break;
		}
	}
}

static void*
sprite_system_get_component(struct System *self, Entity entity)
{
	struct SpriteSysData *ssd = self->data;
	struct Sprite *spr;
	for (int i = 0; i < ssd->sprites->len; i++) {
		spr = &g_array_index(ssd->sprites, struct Sprite, i);
		if (spr->entity == entity)
			return spr;
	}

	return NULL;
}

void
sprite_system_register()
{
	struct System *sys = g_new0(struct System, 1);
	sys->name = "sprite";
	sys->init = sprite_system_init;
	sys->exit = sprite_system_exit;
	sys->update = sprite_system_update;
	sys->create_component = sprite_system_create_component;
	sys->destroy_component = sprite_system_destroy_component;
	sys->get_component = sprite_system_get_component;

	game_system_register(SPRITE, sys);
}

void
sprite_init(Entity entity, const char *sheet)
{
	g_assert_nonnull(sheet);

	struct Sprite *spr = get_sprite(entity);

	// unref existing texture
	if (spr->initialized) {
		ref_counter_dec(spr->texture);
	}

	struct System *sys = spr->creator;
	struct SpriteSysData *ssd = sys->data;
	struct SDL_Renderer *rndr = gfx_system_get_renderer(ssd->systems.gfx);

	SDL_Surface *surf = image_load(ssd->systems.img, sheet);
	SDL_Texture *tex = SDL_CreateTextureFromSurface(rndr, surf);

	if (!tex) {
		game_log(
			LOG_FATAL,
			"failed to create texture from image '%s'\n%s",
			sheet,
			SDL_GetError()
		);
	}
	SDL_FreeSurface(surf);

	spr->texture = ref_counter_new(tex, destroy_sdl_texture);
	spr->initialized = true;
}

void
sprite_clone(Entity src, Entity dst)
{
	g_assert(src != dst);

	struct Sprite *src_spr = get_sprite(src);
	g_assert_true(src_spr->initialized);

	struct Sprite *dst_spr = get_sprite(dst);

	memcpy(dst_spr, src_spr, sizeof(struct Sprite));

	dst_spr->entity = dst;
	dst_spr->current_animation = NULL;
	dst_spr->texture = ref_counter_inc(src_spr->texture);

	// increment the refcount of the frameset GArray;
	// this one is read-only and can be shared by multiple sprites
	dst_spr->frameset = g_array_ref(src_spr->frameset);

	dst_spr->animations = g_hash_table_new_full(
		g_str_hash,
		g_str_equal,
		NULL,
		destroy_animation
	);

	// deep-copy the animations hash table
	GHashTableIter i;
	gpointer k, v;
	g_hash_table_iter_init(&i, src_spr->animations);

	while (g_hash_table_iter_next(&i, &k, &v)) {
		struct Animation *src_anim = v;
		struct Animation *dst_anim = g_memdup(
			src_anim, sizeof(struct Animation)
		);

		// increment the refcount of the frames GArray
		dst_anim->frames = g_array_ref(src_anim->frames);

		// reset animation state
		dst_anim->paused = false;
		dst_anim->last_update = 0;
		dst_anim->current_frame = 0;

		// store the copied animation into hash table
		g_hash_table_insert(dst_spr->animations, k, dst_anim);
	}
}


void
sprite_position_set(Entity entity, int x, int y)
{
	struct Sprite *spr = get_sprite(entity);
	spr->x = x;
	spr->y = y;
}

void
sprite_position_get(Entity entity, int *x, int *y)
{
	g_assert_nonnull(x);
	g_assert_nonnull(y);

	struct Sprite *spr = get_sprite(entity);
	*x = spr->x;
	*y = spr->y;
}

void
sprite_frameset_set(Entity entity, int *frameset, int count)
{
	g_assert_nonnull(frameset);
	g_assert(count > 0);

	struct Sprite *spr = get_sprite(entity);

	// decrement the refcount for current framest GArray before overwriting
	// it, cause it could be shared between multiple sprites
	if (spr->frameset)
		g_array_unref(spr->frameset);

	spr->frameset = g_array_sized_new(
		false, false, count, sizeof(SDL_Rect)
	);

	for (int i = 0; i < count; i++) {
		SDL_Rect *r = &g_array_index(spr->frameset, SDL_Rect, i);
		r->x = frameset[i * 4];
		r->y = frameset[i * 4 + 1];
		r->w = frameset[i * 4 + 2];
		r->h = frameset[i * 4 + 3];
	}
}

void
sprite_animation_add(
	Entity entity,
	const char *name,
	int *frames,
	int count,
	int duration
) {
	g_assert_nonnull(name);
	g_assert(strlen(name) != 0);
	g_assert_nonnull(frames);
	g_assert(count > 0);
	g_assert(duration >= 0);

	struct Sprite *spr = get_sprite(entity);
	struct Animation *anim = g_new(struct Animation, 1);

	anim->frames = g_array_sized_new(false, false, count * 4, sizeof(int));
	memcpy(anim->frames->data, frames, sizeof(int) * count * 4);
	anim->frames->len = count;

	anim->duration = duration;
	anim->current_frame = 0;
	anim->last_update = 0;
	anim->paused = false;

	g_hash_table_insert(spr->animations, (char*)name, anim);
}

void
sprite_animation_play(Entity entity, const char *name)
{
	g_assert_nonnull(name);

	struct Sprite *spr = get_sprite(entity);
	struct Animation *anim = g_hash_table_lookup(spr->animations, name);
	if (!anim) {
		game_log(
			LOG_FATAL,
			"sprite %d does not have '%s' animation\n",
			entity,
			name
		);
	}

	anim->current_frame = 0;
	anim->last_update = 0;
	anim->paused = false;
	spr->current_animation = anim;
}

void
sprite_animation_pause(Entity entity)
{
	struct Sprite *spr = get_sprite(entity);
	if (spr->current_animation)
		spr->current_animation->paused = true;
}

bool
sprite_animation_is_paused(Entity entity)
{
	struct Sprite *spr = get_sprite(entity);
	if (spr->current_animation)
		return spr->current_animation->paused;
	return false;
}

void
sprite_animation_resume(Entity entity)
{
	struct Sprite *spr = get_sprite(entity);
	if (spr->current_animation)
		spr->current_animation->paused = false;
}

void
sprite_visibility_set(Entity entity, bool visible)
{
	struct Sprite *spr = get_sprite(entity);
	spr->visible = visible;
}


void
sprite_zorder_set(Entity entity, int z)
{
	struct Sprite *spr = get_sprite(entity);
	struct SpriteSysData *ssd = spr->creator->data;
	spr->z = z;
	g_array_sort(ssd->sprites, sprite_z_cmp);
}

int
sprite_zorder_get(Entity entity)
{
	struct Sprite *spr = get_sprite(entity);
	return spr->z;
}
