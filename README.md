# BOMBERMAN
This is a clone of the famous Bomberman game, created for educational purposes
by Ivan Nikolaev. It's in public domain, although some of the sprites used here
are ripped from the original game or have been created by others and all rights
belong to them. For any issue, please send an email to voidexp@gmail.com.

## Requirements

    * `spark` engine, built in the parent directory
    * `SDL2`
    * `SDL2-Image`
    * `GLib`
    * `CMake` (used by Chipmunk as build system)

## Building

The game uses a third party A* algorithm (BigZaphod's C implementation) and
Chipmunk 7.0 physics middleware. Those are included as git submodules, which
can be initialized and checked out with these commands:

    `$ git submodule init`
    `$ git submodule update`

When all requirements are satisfied, the plain

    `$ make`

command should to the rest.
